---
title: My Second Nuxt Content Article
description: This is another article on Vue, Nuxt and Nuxt Content that goes into some advanced topics
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Blandit aliquam etiam erat velit scelerisque in. Cras sed felis eget velit aliquet sagittis id consectetur purus. Imperdiet nulla malesuada pellentesque elit eget gravida cum. Pulvinar neque laoreet suspendisse interdum consectetur. Tortor posuere ac ut consequat semper. Eget nullam non nisi est sit amet. Sapien et ligula ullamcorper malesuada proin libero. Ullamcorper malesuada proin libero nunc consequat interdum varius. Lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi. Morbi tincidunt ornare massa eget egestas. Mattis rhoncus urna neque viverra. Enim nulla aliquet porttitor lacus luctus accumsan tortor posuere. Eget nulla facilisi etiam dignissim diam quis enim.

1. Malesuada fames
1. Eget nullam non nisi
1. Morbi tincidunt ornare

Malesuada fames ac turpis egestas maecenas. Eget dolor morbi non arcu risus quis varius. Pulvinar mattis nunc sed blandit. Accumsan sit amet nulla facilisi morbi tempus iaculis. Non odio euismod lacinia at quis risus sed. Montes nascetur ridiculus mus mauris vitae ultricies. Neque viverra justo nec ultrices dui. Ac auctor augue mauris augue neque gravida in fermentum et. Nullam non nisi est sit amet facilisis magna. Id aliquet risus feugiat in ante. Fames ac turpis egestas sed tempus urna et. Porta nibh venenatis cras sed felis eget velit. Odio facilisis mauris sit amet massa. Ut morbi tincidunt augue interdum velit. Dui accumsan sit amet nulla facilisi morbi. Tellus mauris a diam maecenas sed enim ut sem viverra.

- **Aliquam purus** sit amet luctus venenatis lectus magna.
- **Tempor nec feugiat** <span class="text-co-blue">nisl pretium fusce id velit ut tortor</span>.
- Arcu felis bibendum ut tristique et egestas quis.

Arcu bibendum at varius vel pharetra. Turpis tincidunt id aliquet risus feugiat. Pellentesque pulvinar pellentesque habitant morbi tristique. Dictum at tempor commodo ullamcorper a lacus vestibulum. Quis ipsum suspendisse ultrices gravida dictum fusce. Sit amet nisl suscipit adipiscing. Enim nulla aliquet porttitor lacus luctus accumsan tortor. Donec massa sapien faucibus et molestie ac feugiat sed. Adipiscing diam donec adipiscing tristique risus nec feugiat. Mauris ultrices eros in cursus turpis massa. Lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare. Faucibus a pellentesque sit amet porttitor. Ornare aenean euismod elementum nisi quis eleifend quam adipiscing. Bibendum at varius vel pharetra vel.

Egestas quis ipsum suspendisse ultrices gravida dictum fusce ut. Vulputate sapien nec sagittis aliquam malesuada bibendum arcu vitae. Ultrices vitae auctor eu augue ut. Cursus metus aliquam eleifend mi in nulla posuere. Tellus in hac habitasse platea dictumst. Imperdiet sed euismod nisi porta lorem mollis. Consectetur a erat nam at lectus urna duis convallis. Velit scelerisque in dictum non consectetur a erat. Eget arcu dictum varius duis at consectetur. Aliquet enim tortor at auctor urna nunc id cursus. Augue lacus viverra vitae congue. Id ornare arcu odio ut sem nulla pharetra. At elementum eu facilisis sed odio morbi quis. Non diam phasellus vestibulum lorem sed risus ultricies. Scelerisque varius morbi enim nunc faucibus.

Phasellus faucibus scelerisque eleifend donec pretium. Erat pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Dui sapien eget mi proin sed libero enim sed faucibus. Vulputate mi sit amet mauris commodo quis. Mus mauris vitae ultricies leo integer. Proin fermentum leo vel orci porta. Non blandit massa enim nec dui nunc mattis enim. Duis ut diam quam nulla porttitor massa. Dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu. In ornare quam viverra orci sagittis eu volutpat odio.
